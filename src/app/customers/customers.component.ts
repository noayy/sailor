import { AuthService } from './../auth.service';
import { ClassifyService } from './../classify.service';
import { CustomerService } from './../customer.service';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Customer } from '../interfaces/customer';
import { CSVRecord } from '../CSVModel';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {


  customers=[];
  displayedColumns: string[] = ['name','phone','email','predict'];
  customers$;
  new;
  resultPredict;

  rowToEdit:number = -1; //אתחול אינדקס השורה עם מינוס 1 כדי שלא נהיה במצב של עריכה בשום שורה
  customerToEdit:Customer = {
  id:null,
  age:null,
  more_than_1_card:null,
  student:null,
  main_city_sail:null,
  // Utilizing_card_1:null,
  // Utilizing_card_2:null,
  // Utilizing_card_3:null,
  // Utilizing_card_4:null,
  Utilizing_card:null,
  S_Utilizing_card:null,
  perc_private:null,
   perc_proff:null,
   name:null,
   email:null,
   phone:null
  };

  addBookFormOpen = false;

  lastCustomerArrived //דפדוף
  firstCustomerArrived;//דפדוף

  @Input() name:string;
  @Input() email:string;
  @Input() phone:string;

  moveToEditState(index){ //הפונקציה מאתרת את מספר השורה שבה הפריט שנעדכן נמצא
    this.customerToEdit.age = this.customers[index].age;
    this.customerToEdit.more_than_1_card = this.customers[index].more_than_1_card;
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.email = this.customers[index].email;
    this.customerToEdit.phone = this.customers[index].phone;
    this.rowToEdit = index; 
  }


  constructor(private db:AngularFirestore, private TableCustomerService:CustomerService, private ClassifyService:ClassifyService, public authService:AuthService) { }

  customersCollection:AngularFirestoreCollection = this.db.collection('customers'); 

  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.TableCustomerService.editCustomer(id,this.customerToEdit.more_than_1_card, this.customerToEdit.name,
      this.customerToEdit.email,this.customerToEdit.phone);
    this.rowToEdit = null;
  }

  predict(){
    this.ClassifyService.classify(this.customers).subscribe(
                   (res:number[])=> {
                     console.log(res);
                    //  if (res > 0.5){
                    //    customer.predict = 'will not leave';
                    //  }
                    //  if (res < 0.5) {
                    //    customer.predict = 'will leave';
                    //  }
                    for (let i=0;i<res.length; i++){
                      const customerPrediction = res[i];
                      const predict = customerPrediction>0.5?'will not leave':'will leave';
                      const customer:Customer = this.customers[i];
                      const {id} = customer;
                      this.TableCustomerService.updateCustomer(id, predict);
                      if (predict=='will leave'){
                        this.ClassifyService.classify2(this.customers[i].email,this.customers[i].name).subscribe(
                          (res:number[])=> {
                            console.log(res);
                           }
                        )   
                      }
                    }
                    }
                 )         
  }


  deleteCustomer(){
    for (let i=0;i<this.customers.length; i++){
      let id = this.customers[i].id;
      this.TableCustomerService.deleteCustomer(id);
    }
    this.ngOnInit();

  }

  ngOnInit(): void {

    this.customers$ = this.TableCustomerService.getCustomers(null);
    this.customers$.subscribe(
      docs => {  
        this.lastCustomerArrived = docs[docs.length-1].payload.doc;       
        this.customers = [];
          for (let document of docs) {
          const customer:Customer = document.payload.doc.data();
          customer.id = document.payload.doc.id;
             this.customers.push(customer); 
        }   
               
      }
      
    )
}

  //דפדוף
nextPage(){
  this.customers$ = this.TableCustomerService.getCustomers(this.lastCustomerArrived); 
  this.customers$.subscribe( 
    docs => { 
      this.lastCustomerArrived = docs[docs.length-1].payload.doc; 
      this.firstCustomerArrived = docs[0].payload.doc; 
      this.customers = []; 
      for(let document of docs){ 
        const customer:Customer = document.payload.doc.data(); 
        customer.id = document.payload.doc.id; 
        this.customers.push(customer); 
      } 
    }
    )
    }



//דפדוף
previewsPage(){
this.customers$ = this.TableCustomerService.getCustomers2(this.firstCustomerArrived); 
this.customers$.subscribe( 
docs => { 
  this.lastCustomerArrived = docs[docs.length-1].payload.doc; 
  this.firstCustomerArrived = docs[0].payload.doc; 
  this.customers = []; 
  for(let document of docs){ 
    const customer:Customer = document.payload.doc.data(); 
    customer.id = document.payload.doc.id; 
    this.customers.push(customer); 
  } 
}
)
}



public records: any[] = [];
@ViewChild('csvReader') csvReader: any;

uploadListener($event: any): void {

  let text = [];
  let files = $event.srcElement.files;

  if (this.isValidCSVFile(files[0])) {

    let input = $event.target;
    let reader = new FileReader();
    reader.readAsText(input.files[0]);

    reader.onload = () => {
      let csvData = reader.result;
      let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);

      let headersRow = this.getHeaderArray(csvRecordsArray);

      this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);
      console.log(this.records);

      for (let i=0;i<this.records.length; i++){
        const result = this.records[i];
        const result2={name:this.records[i].name,
                        email:this.records[i].email,
                        phone:this.records[i].phone,
                        age:this.records[i].age,
                        more_than_1_card:this.records[i].more_than_1_card,
                        main_city_sail:this.records[i].main_city_sail,
                        student:this.records[i].student,
                        // Utilizing_card_1:this.records[i].Utilizing_card_1,
                        // Utilizing_card_2:this.records[i].Utilizing_card_2,
                        // Utilizing_card_3:this.records[i].Utilizing_card_3,
                        // Utilizing_card_4:this.records[i].Utilizing_card_4,
                        Utilizing_card:this.records[i].Utilizing_card,
                        S_Utilizing_card:this.records[i].S_Utilizing_card,
                        perc_private:this.records[i].perc_private,
                        perc_proff:this.records[i].perc_proff,
                      };
        console.log(result2);
        this.customersCollection.add(result2);
      }
    };

    reader.onerror = function () {
      console.log('error is occured while reading file!');
    };

  } else {
    alert("Please import valid .csv file.");
    this.fileReset();
  }
  this.ngOnInit();
}

getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
  let csvArr = [];

  for (let i = 1; i < csvRecordsArray.length; i++) {
    let curruntRecord = (<string>csvRecordsArray[i]).split(',');
    if (curruntRecord.length == headerLength) {
      let csvRecord: CSVRecord = new CSVRecord();
      csvRecord.name = curruntRecord[0].trim();
      csvRecord.email = curruntRecord[1].trim();
      csvRecord.phone = curruntRecord[2].trim();

      csvRecord.age = curruntRecord[3].trim();
      csvRecord.more_than_1_card = curruntRecord[4].trim();
      csvRecord.main_city_sail = curruntRecord[5].trim();
      csvRecord.student = curruntRecord[6].trim();
      // csvRecord.Utilizing_card_1 = curruntRecord[7].trim();
      // csvRecord.Utilizing_card_2 = curruntRecord[8].trim();
      csvRecord.Utilizing_card = curruntRecord[7].trim();
      csvRecord.perc_private = curruntRecord[8].trim();
      csvRecord.perc_proff = curruntRecord[9].trim();
      csvRecord.S_Utilizing_card = curruntRecord[10].trim();


      csvArr.push(csvRecord);
    }
  }
  return csvArr;
}

isValidCSVFile(file: any) {
  return file.name.endsWith(".csv");
}

getHeaderArray(csvRecordsArr: any) {
  let headers = (<string>csvRecordsArr[0]).split(',');
  let headerArray = [];
  for (let j = 0; j < headers.length; j++) {
    headerArray.push(headers[j]);
  }
  return headerArray;
}

fileReset() {
  this.csvReader.nativeElement.value = "";
  this.records = [];
}


}