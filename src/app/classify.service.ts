import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { EmailValidator } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Customer } from './interfaces/customer';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  private url2 = "https://q4nw4va38b.execute-api.eu-west-1.amazonaws.com/insert-users"

  classify2(email:string,name:string):Observable<any>{
    let json = {
      "body": [
          {
          "email": email,
          "first_name": name,
          "last_name": "sailor member",
          "status": 0
          }
      ]
  };

  // let json2 = JSON.stringify(json);
    return this.http.post<any>(this.url2,json).pipe(
      map(res => {
        console.log(res);
        console.log(json);
        let final = res.body;
        console.log(final); 
        return final;
      })
    );
  }

  //Api get way URL (connect to lamda function call "test")
 // private url = "https://jm4tqi1wul.execute-api.us-east-1.amazonaws.com/beta"
 private url = "https://2n661lzq2m.execute-api.us-east-1.amazonaws.com/model"
  
  classify(customers:Customer[]):Observable<any>{


      let json = {
        'data':customers
      }

      let body = JSON.stringify(json);
      console.log(body)
   return this.HttpClient.post<any>(this.url, body).pipe(
        map(res => {
          console.log(res); 
          let final:string = res.body;
          return final; 
        })
      )
  
    }


  constructor(private HttpClient:HttpClient,private http:HttpClient) { }

}
