import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

 //נגדיר משתנים
 hide = true; //תרגיל בית 8 נוסיף משתנה בשביל העין של הסיסמא בטופס
 email:string;
 password:string;
 errorMessage:string; //הרצאה 9
 isError:boolean = false; //הדיפולט יהיה פאלס
 
 onSubmit(){
   this.authService.SignUp(this.email,this.password)
   .then(res => 
     {
       console.log('Succesful sign up',res);
       this.router.navigate(['/customers']); 
     }
   ).catch(
     err => {
       console.log(err); //נדפיס את התשובה לקונסול בשביל דיבאגינג
       this.isError = true; //יגרום להודעת השגיאה להיות מוצגת, לפי התנאי שרשמנו בטיימפלייט
       this.errorMessage = err.message;
     }
   );
 }

 constructor(private authService:AuthService,
   private router:Router) { }

 ngOnInit(): void {
 }

}
