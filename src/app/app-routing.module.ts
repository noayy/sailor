import { LeaveCustomerComponent } from './leave-customer/leave-customer.component';
import { LoginComponent } from './login/login.component';
import { CustomersComponent } from './customers/customers.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignupComponent } from './signup/signup.component';
import { AddCustomerComponent } from './add-customer/add-customer.component';
import { WelcomeComponent } from './welcome/welcome.component';

const routes: Routes = [
  { path: 'customers', component: CustomersComponent },
  // { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'leaves', component: LeaveCustomerComponent },
  { path: 'add', component: AddCustomerComponent },
  { path: 'welcome', component: WelcomeComponent },

  { path: '',   redirectTo: '/welcome', pathMatch: 'full' }, // redirect to `first-component`


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
