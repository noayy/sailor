import { Component, ViewChild } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { CSVRecord } from './CSVModel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'finalproject';

  public records: any[] = [];
  @ViewChild('csvReader') csvReader: any;

  uploadListener($event: any): void {

    let text = [];
    let files = $event.srcElement.files;

    if (this.isValidCSVFile(files[0])) {

      let input = $event.target;
      let reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        let csvData = reader.result;
        let csvRecordsArray = (<string>csvData).split(/\r\n|\n/);

        let headersRow = this.getHeaderArray(csvRecordsArray);

        this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);
        console.log(this.records);
        for (let i=0;i<this.records.length; i++){
          const result = this.records[i];
          const result2={age:this.records[i].age,
                          more_than_1_card:this.records[i].more_than_1_card,
                          main_city_sail:this.records[i].main_city_sail,
                          student:this.records[i].student,
                         // Utilizing_card_1:this.records[i].Utilizing_card_1,
                         // Utilizing_card_2:this.records[i].Utilizing_card_2,
                          Utilizing_card:this.records[i].Utilizing_card,
                          S_Utilizing_card:this.records[i].S_Utilizing_card,
                          perc_private:this.records[i].perc_private,
                          perc_proff:this.records[i].perc_proff,
                        };
          console.log(result2);
          this.customersCollection.add(result2);
        }
      };

      reader.onerror = function () {
        console.log('error is occured while reading file!');
      };

    } else {
      alert("Please import valid .csv file.");
      this.fileReset();
    }
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    let csvArr = [];

    for (let i = 1; i < csvRecordsArray.length; i++) {
      let curruntRecord = (<string>csvRecordsArray[i]).split(',');
      if (curruntRecord.length == headerLength) {
        let csvRecord: CSVRecord = new CSVRecord();
        csvRecord.name = curruntRecord[0].trim();
        csvRecord.email = curruntRecord[1].trim();
        csvRecord.phone = curruntRecord[2].trim();
  
        csvRecord.age = curruntRecord[3].trim();
        csvRecord.more_than_1_card = curruntRecord[4].trim();
        csvRecord.main_city_sail = curruntRecord[5].trim();
        csvRecord.student = curruntRecord[6].trim();
        // csvRecord.Utilizing_card_1 = curruntRecord[7].trim();
        // csvRecord.Utilizing_card_2 = curruntRecord[8].trim();
        csvRecord.Utilizing_card = curruntRecord[7].trim();
        csvRecord.perc_private = curruntRecord[8].trim();
        csvRecord.perc_proff = curruntRecord[9].trim();
        csvRecord.S_Utilizing_card = curruntRecord[10].trim();
        csvArr.push(csvRecord);
      }
    }
    return csvArr;
  }

  isValidCSVFile(file: any) {
    return file.name.endsWith(".csv");
  }

  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string>csvRecordsArr[0]).split(',');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  fileReset() {
    this.csvReader.nativeElement.value = "";
    this.records = [];
  }
  
  constructor(private db:AngularFirestore) { }

  customersCollection:AngularFirestoreCollection = this.db.collection('customers'); 

  addcustomer22(id,firstName){
    const customer2 = {id:id,name:firstName}; 
    this.customersCollection.add(customer2);
  }

}
